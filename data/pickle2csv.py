#!/usr/bin/python2
# This little script transform from pickle format to CSV format.
# Author: Ivan Pulido

import csv
import cPickle

ifile1 = open('angulos_adelante.pickle', "rb")
ifile2 = open('angulos_adelante_circular.pickle', "rb")
ifile3 = open('angulos_atras.pickle', "rb")
ifile4 = open('angulos_atras_circular.pickle', "rb")

ofile1 = open('angles_forward.csv',"wb")
ofile2 = open('angles_forward_circular.csv',"wb")
ofile3 = open('angles_backwards.csv',"wb")
ofile4 = open('angles_backwards_circular.csv',"wb")

angles1 = cPickle.load(ifile1)
angles2 = cPickle.load(ifile2)
angles3 = cPickle.load(ifile3)
angles4 = cPickle.load(ifile4)

writer1 = csv.writer(ofile1, delimiter='\t')
writer2 = csv.writer(ofile2, delimiter='\t')
writer3 = csv.writer(ofile3, delimiter='\t')
writer4 = csv.writer(ofile4, delimiter='\t')

for row in angles1:
  writer1.writerow(row)
for row in angles2:
  writer2.writerow(row)
for row in angles3:
  writer3.writerow(row)
for row in angles4:
  writer4.writerow(row)

ifile1.close()
ifile2.close()
ifile3.close()
ifile4.close()
ofile1.close()
ofile2.close()
ofile3.close()
ofile4.close()
