from pylab import *
import scipy.ndimage as nd
import cPickle
import time
t1 = time.time()
# Directories where the images are stored - Adjust to your needs
ruta1='/run/media/ivan/DatosIvan/images_N2000/'
ruta2='/run/media/ivan/DatosIvan/images_N2000_circular/'
ruta3='/run/media/ivan/DatosIvan/images_N2000_atras/'
ruta4='/run/media/ivan/DatosIvan/images_N2000_atras_circular/'
rutas=[ruta1,ruta2,ruta3,ruta4]
N = 44  # number of measurements (subdirectories) - adjust to your needs
for k in range(len(rutas)):
  ruta=rutas[k]
  angulos=[]
  for j in range(1,N):
    subdir='medicion'+str(j)+'/'  # this shows how subdirectories are called
    ruta2=ruta+subdir
    angulo=[]
    for i in range(2000):
      im = imread(ruta2+'im'+str(i)+'.tif')[800:,512:] # this shows how images are called
      im1 = im[1,:]
      fu=fft(im1-im1.mean())
      n=abs(fu[5:15]).argmax()+5
      angulo.append(angle(fu[n]))
    angulos.append(angulo)
    # Save data
    filename = 'angulos'+str(k)+'.pickle'
    f = open(filename,"w")
    cPickle.dump(angulos,f)
t2 = time.time()
print "Execution time:",t2-t1
