This little project aims to create code used to characterize a linear stage
made by Zaber which are commonly used in optics labs. So far it has only been
tested on the T-LSM series. It uses python 2.7.x with its scientific suite,
that is, numpy, scipy, matplotlib, pyserial and ipython (specially the 
ipython notebook interface).

The file zabercomm.py contains a modified version of what can be found in the
Zaber wiki pages for Python and it's the code charged of communicating with 
the linear stage (sending/receiving instructions and commands).

The actual data was gathered using ipython notebooks which can be found in
the notebooks directory. The Adquisition.ipynb notebook was the one used
to gather the data/images directly from the experiment (using the digital
camera from thorlabs, via the pyueye software to control it).

The file data_preprocessing.py was the script used to intially process all
the data so that it filtered only what was needed. It could take a lot of time
depending on how much data you have stored. The filtered data can be found in
the data directory in CVS and pickle formats (there's also a script to change
the format from pickle to CVS).

Lastly, you can find inside the notebook directory the actual ipython notebook
used to manipulate data, do the numerical analysis/computations and show the
results.
