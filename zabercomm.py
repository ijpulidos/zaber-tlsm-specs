#!/usr/bin/env python

import serial, sys, time, glob

def scan():
   # scan for available ports. return a list of device names
   return glob.glob('/dev/ttyS*') + glob.glob('/dev/ttyUSB*')

def send(inst):
   # send instruction
   # inst must be a list of 6 bytes (no error checking)
   for i in range (6):
       ser.write(chr(inst[i]))
   return

def receive():
   # return 6 bytes from the receive buffer
   # there must be 6 bytes to receive (no error checking)
   r = [0,0,0,0,0,0]
   for i in range (6):
       #print "*",i,r[i]	
       r[i] = ord(ser.read(1))
      

   return r
 
def gohome():
  # puts linear stage into home position
  instruction = [0,1,0,0,0,0]
  send(instruction)
  reply=receive()

def moveabs(n):
  # puts linear stage in the given absolute position n
  # in millimeters
  P = int(n/47.625e-6)
  P0 = P%256
  P1 = (P/256)%256
  P2 = ((P/256)/256)%256
  P3 = (((P/256)/256)/256)%256

# open serial port
# replace "/dev/ttyUSB0" with "COM1", "COM2", etc in Windows
try:
# Change this if you use "pure" serial instead of USB
   ser = serial.Serial("/dev/ttyUSB0", 9600, 8, 'N', 1, timeout=5)   
#   ser = serial.Serial("/dev/ttyS0", 9600, 8, 'N', 1, timeout=5)   
except:
   print "Error opening com port. Quitting."
   sys.exit(0)
